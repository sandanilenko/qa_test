# -*- coding: utf-8 -*-
import json
import unittest
from os import popen, path, sep


# кодеки: h.264/avc, h.265/hevc

# ● разрешения: 720x480, 1920x1080

# ● фрейм­рейт (framerate): 30, 60

# ● бит­рейт (bitrate):

# ○ для 720x480: 1000 Mbps, 2000 Mbps

# ○ для 1920x1080: 5000 Mbps, 8000 Mbps

class TestCaseH264Scale720X480R30B1000MB(unittest.TestCase):
    final_file_name = path.join(sep, 'tmp', 'H264Scale720X480R30B1000MB_output.mp4')
    true_codec = 'h264'
    codec_code = 'libx264'
    true_width = 720
    true_height = 480
    true_frame_rate = 30
    true_bit_rate = 1000

    @classmethod
    def setUpClass(cls):
        popen('ffmpeg -i http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_60fps_stereo_abl.mp4'
              ' -vf scale={width}:{height} -r {frame_rate}  -c:v {codec_code} -b:v {bit_rate}M -t 10 '
              '{final_file_name}'.format(width=cls.true_width, height=cls.true_height,
                                         codec_code=cls.codec_code, bit_rate=cls.true_bit_rate,
                                         frame_rate=cls.true_frame_rate, final_file_name=cls.final_file_name)).read()

        params = popen('ffprobe -v quiet -print_format json '
                       '-show_streams -show_format {0}'.format(cls.final_file_name)).read()
        cls.init_params(json.loads(params))

    @classmethod
    def init_params(cls, params):
        cls.codec = params['streams'][0]['codec_name']
        cls.width = params['streams'][0]['width']
        cls.height = params['streams'][0]['height']
        cls.frame_rate = params['streams'][0]['r_frame_rate']
        cls.bit_rate = params['streams'][0]['bit_rate']

    def test_codec(self):
        self.assertEqual(self.__class__.codec, self.__class__.true_codec)

    def test_scale(self):
        self.assertEqual(self.__class__.width, self.__class__.true_width)
        self.assertEqual(self.__class__.height, self.__class__.true_height)

    def test_frame_rate(self):
        self.assertEqual(self.__class__.frame_rate, str(self.__class__.true_frame_rate) + '/1')

    def test_bit_rate(self):
        error = self.__class__.true_bit_rate * 0.02
        bit_rate_l = self.__class__.true_bit_rate - error
        bit_rate_r = self.__class__.true_bit_rate + error
        # print bit_rate_l, int(self.__class__.bit_rate) / 1024, bit_rate_r
        self.assertTrue(bit_rate_l * 1024 <= self.__class__.bit_rate <= bit_rate_r * 1024)



class TestCaseH264Scale720X480R30B2000MB(TestCaseH264Scale720X480R30B1000MB):
    final_file_name = path.join(sep, 'tmp', 'H264Scale720X480R30B2000MB_output.mp4')
    true_bit_rate = 2000


class TestCaseH264Scale720X480R60B1000MB(TestCaseH264Scale720X480R30B1000MB):
    final_file_name = path.join(sep, 'tmp', 'H264Scale720X480R60B1000MB_output.mp4')
    true_frame_rate = 60


class TestCaseH264Scale720X480R60B2000MB(TestCaseH264Scale720X480R60B1000MB):
    final_file_name = path.join(sep, 'tmp', 'H264Scale720X480R60B2000MB_output.mp4')
    true_bit_rate = 2000


class TestCaseH265Scale720X480R30B1000MB(TestCaseH264Scale720X480R30B1000MB):
    final_file_name = path.join(sep, 'tmp', 'H265Scale720X480R30B1000MB_output.mp4')
    true_codec = 'hevc'
    codec_code = 'libx265'


class TestCaseH265Scale720X480R30B2000MB(TestCaseH265Scale720X480R30B1000MB):
    final_file_name = path.join(sep, 'tmp', 'H265Scale720X480R30B2000MB_output.mp4')
    true_bit_rate = 2000


class TestCaseH265Scale720X480R60B1000MB(TestCaseH265Scale720X480R30B1000MB):
    final_file_name = path.join(sep, 'tmp', 'H265Scale720X480R60B1000MB_output.mp4')
    true_frame_rate = 60


class TestCaseH265Scale720X480R60B2000MB(TestCaseH265Scale720X480R60B1000MB):
    final_file_name = path.join(sep, 'tmp', 'H265Scale720X480R60B2000MB_output.mp4')
    true_bit_rate = 2000


class TestCaseH264Scale1920X1080R30B5000MB(TestCaseH264Scale720X480R30B1000MB):
    final_file_name = path.join(sep, 'tmp', 'H264Scale1920X1080R30B5000MB_output.mp4')
    true_width = 1920
    true_height = 1080
    true_bit_rate = 5000


class TestCaseH264Scale1920X1080R30B8000MB(TestCaseH264Scale1920X1080R30B5000MB):
    final_file_name = path.join(sep, 'tmp', 'H264Scale1920X1080R30B8000MB_output.mp4')
    true_bit_rate = 8000


class TestCaseH264Scale1920X1080R60B5000MB(TestCaseH264Scale1920X1080R30B5000MB):
    final_file_name = path.join(sep, 'tmp', 'H264Scale1920X1080R60B5000MB_output.mp4')
    true_frame_rate = 60


class TestCaseH264Scale1920X1080R60B8000MB(TestCaseH264Scale1920X1080R60B5000MB):
    final_file_name = path.join(sep, 'tmp', 'H264Scale1920X1080R60B8000MB_output.mp4')
    true_bit_rate = 8000


class TestCaseH265Scale1920X1080R30B5000MB(TestCaseH264Scale1920X1080R30B5000MB):
    final_file_name = path.join(sep, 'tmp', 'H265Scale1920X1080R30B5000MB_output.mp4')
    true_codec = 'hevc'
    codec_code = 'libx265'


class TestCaseH265Scale1920X1080R30B8000MB(TestCaseH265Scale1920X1080R30B5000MB):
    final_file_name = path.join(sep, 'tmp', 'H265Scale1920X1080R30B8000MB_output.mp4')
    true_bit_rate = 8000


class TestCaseH265Scale1920X1080R60B5000MB(TestCaseH265Scale1920X1080R30B5000MB):
    final_file_name = path.join(sep, 'tmp', 'H265Scale1920X1080R60B5000MB_output.mp4')
    true_frame_rate = 60


class TestCaseH265Scale1920X1080R60B8000MB(TestCaseH265Scale1920X1080R60B5000MB):
    final_file_name = path.join(sep, 'tmp', 'H265Scale1920X1080R60B8000MB_output.mp4')
    true_bit_rate = 8000
